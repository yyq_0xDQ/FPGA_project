`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2018/10/02 15:34:13
// Design Name: 
// Module Name: BPM_calculate
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module BPM_calculate(
    input clk,
    input rst,
    input [15:0]G_ROI_sum,
   // input [15:0]H_ROI,
    input flag, //确定窗口类型，flag = 1位32s窗口，flag = 0 为16s窗口
    output [10:0]HR,
    output HR_valid
    );

    wire clk_sample;
    wire [15:0]G_fft_re,G_fft_im,H_fft_re,H_fft_im; //连接FFT后的实部与虚部 
    wire [15:0]G_ROI_sum_filted;
    wire [15:0]G_ROI_sum_filted_buf,H_ROI_buf; //从缓存模块输出的数据，连接到FFT输入
    wire databuf_mvalid; //缓存数据有效信号
    wire fft_out_valid_G,fft_out_valid_H;//FFT输出有效信号
    wire[31:0] z_score_G,z_score_H;//Z值计算的输出结果，高16位为整数位，高16位的最高位为整数的符号位，低16位为小数位，低16位的最高位为小数的符号位
    wire z_score_G_valid,z_score_H_valid;  //Z值数据有效信号
    wire[15:0] config_fft; 
    
    assign config_fft = (flag)?16'b0000000_1000_00111:16'b0000000_1000_00110; // FFT 输入配置  后五位配置FFT的点数  做 128(flga = 1) 或 64(flag = 0) 点FFT

//生成采样时钟，采样频率为 4 HZ    
clk_divide clk_divide_sample (
    .clk(clk),
    .rst(rst),
    .divide_num(27'd2500_0000),
    .clk_div(clk_sample)
    );
    
//带通滤波器，论文里提供的，MASK 为[-1,0,1],因为仿真时候发现使用这个带通滤波器会使结果与正确值偏差变大，所以暂时没有使用        
/*bp_filter bp_filter_inst(
    .clk(clk_sample),
    .rst(rst),
    .G_mean_b(G_ROI_sum),
    .G_mean_a(G_ROI_sum_filted)
    );*/

//输入的G值数据缓存，输入数据时钟为采样时钟（4 HZ），输出数据时钟为系统时钟 （100 MHZ），每一秒输出一次数据帧（16s的窗口数据或32s的窗口数据，由FLAG的值确定）
fft_databuf fft_databuf_G(
    .clk(clk),
    .clk_sample(clk_sample),
    .data_in(G_ROI_sum),
    .flag(flag),
    .data_out(G_ROI_sum_filted_buf),
    .mvalid(databuf_mvalid)
    );
 /*    
//H通道缓存    
fft_databuf fft_databuf_H(
    .clk(clk),
    .clk_sample(clk_sample),
    .data_in(H_ROI),
    .flag(flag),
    .data_out(H_ROI_buf)
   // .mvalid(databuf_mvalid)
    );
*/
//G通道的FFT计算    
xfft_0 fft_G (
    .aclk(clk),                                                // input wire aclk
    .s_axis_config_tdata(config_fft),                  // input wire [23 : 0] s_axis_config_tdata
    .s_axis_config_tvalid(databuf_mvalid),                // input wire s_axis_config_tvalid
    .s_axis_data_tdata({16'd0,G_ROI_sum_filted_buf}),                      // input wire [31 : 0] s_axis_data_tdata
    .s_axis_data_tvalid(databuf_mvalid),                    // input wire s_axis_data_tvalid
    .s_axis_data_tlast(~databuf_mvalid),                      // input wire s_axis_data_tlast
    .m_axis_data_tdata({G_fft_im,G_fft_re}),                      // output wire [31 : 0] m_axis_data_tdata
    .m_axis_data_tvalid(fft_out_valid_G)                    // output wire m_axis_data_tvalid
    ); 
/*    
//H通道的FFT计算    
xfft_0 fft_H (
    .aclk(clk),                                                // input wire aclk
    .s_axis_config_tdata(config_fft),                  // input wire [23 : 0] s_axis_config_tdata
    .s_axis_config_tvalid(databuf_mvalid),                // input wire s_axis_config_tvalid
    .s_axis_data_tdata({16'd0,H_ROI_buf}),                      // input wire [31 : 0] s_axis_data_tdata
    .s_axis_data_tvalid(databuf_mvalid),                    // input wire s_axis_data_tvalid
    .s_axis_data_tlast(~databuf_mvalid),                      // input wire s_axis_data_tlast
    .m_axis_data_tdata({H_fft_im,H_fft_re}),                      // output wire [31 : 0] m_axis_data_tdata
    .m_axis_data_tvalid(fft_out_valid_H)                    // output wire m_axis_data_tvalid
    );
*/
//G通道的Z值计算    
z_score z_score_G_func(
    .clk(clk),
    .fft_im(G_fft_im),
    .fft_re(G_fft_re),
    .valid(fft_out_valid_G),
    .flag(flag),
    .z_score(z_score_G),
    .mvalid(z_score_G_valid)    
    );
/*    
//H通道的Z值计算    
z_score z_score_H_func(
    .clk(clk),
    .fft_im(H_fft_im),
    .fft_re(H_fft_re),
    .valid(fft_out_valid_H),
    .flag(flag),
    .z_score(z_score_H),
    .mvalid(z_score_H_valid)    
    );
*/
//Z值比较，得出最高幅值所在位置，即得出心率值
z_comparison z_comparison_G_func(
    .clk(clk),
    .rst(rst),
    .z_score_G(z_score_G),
   // .z_score_H(z_score_H),
    .z_valid(z_score_G_valid),
    .flag(flag),
    .HR(HR),
    .mvalid(HR_valid)
    );
            
endmodule
