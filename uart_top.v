`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2018/11/20 00:01:12
// Design Name: 
// Module Name: uart_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module uart_top(
    input clk_in1,
    input rst,
    input [10:0]HR_1,
    input HR_valid_1,
    output RS_232_TX,
    input RS_232_RX,
    output RX_done
    );
    wire TX_done;
    wire send_en;
    wire[7:0]data_tx;
    //串口发送 ,波特率9600，8位，无校验   
    uart_tx uart_tx_inst(
        .clk(clk_in1),
        .rst(rst),
        .bps_D(14'd10416),
        .send_en(send_en),
        .data_in(data_tx),
        .RS_232_TX(RS_232_TX),
        .TX_done(TX_done)
      //  .uart_state(uart_state)
        );
    
    //串口发送循环，1s发一次，一次发4个字节，包含一次的心率值    
    tx_cycle tx_cycle_inst (
        .clk(clk_in1),
        .rst(rst),
        .clk_1hz(HR_valid_1),
        .TX_done(TX_done),
        .data_buf({8'd22,1'd0,HR_1[10:4]}),
        .data_tx(data_tx),
        .send_en(send_en)
        );
        
    uart_rx uart_rx_inst(
        .clk(clk_in1),
        .rst(rst),
        .bps_D(14'd10416),
        .RS_232_RX(RS_232_RX),
       // .uart_state_rx(uart_state_rx),
       // .data_byte(data_byte),
        .RX_done(RX_done)
        );
endmodule
