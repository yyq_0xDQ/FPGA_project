`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2018/11/20 00:02:03
// Design Name: 
// Module Name: oled
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module oled(
    input clk_in1,
    input rst,
    input [10:0]HR_1,
    input HR_valid_1,
    input RX_done,
    output sck,
    output miso,
    output reset_oled,
    output dc
    );
    wire[11:0] data_buf_oled;
    wire clk_1hz;
    
display_oled dis_oled_inst(
    .clk(clk_in1),
    .rst(rst),
    .HR({1'd0,HR_1[10:4]}),
    .HR_valid(HR_valid_1),
    .start(RX_done),
    .data_buf(data_buf_oled),
    .clk_1hz_1(clk_1hz)
    );
    
oled_top oled_inst(
    .clk(clk_in1),
    .rst(clk_1hz),
    .dc(dc),
    .sck(sck),
    .miso(miso),
    .reset_oled(reset_oled),
    .data_buf(data_buf_oled)
);
endmodule
