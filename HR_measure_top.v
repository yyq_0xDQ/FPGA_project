`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2018/09/30 15:44:54
// Design Name: 
// Module Name: HR_measure_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module HR_measure_top(
    OV7725_D,
    OV7725_HREF,
    OV7725_PCLK,
    OV7725_SIOC,
    OV7725_SIOD,
    OV7725_VSYNC,
    OV7725_XCLK,
    OV7725_PWDN,
    OV7725_FSIN,
    clk_in1,
    rst,
    vga_blue,
    vga_green,
    vga_hsync,
    vga_red,
    vga_vsync,
    RS_232_TX,
    RS_232_RX,
    sck,
    miso,
    reset_oled,
    dc    
    );
    input [7:0]OV7725_D;
    input OV7725_HREF;
    input OV7725_PCLK;
    output OV7725_SIOC;
    inout OV7725_SIOD;
    input OV7725_VSYNC;
    output OV7725_XCLK;
    output OV7725_PWDN;
    output OV7725_FSIN;
    input clk_in1;
    input rst;
    output [3:0]vga_blue;
    output [3:0]vga_green;
    output vga_hsync;
    output [3:0]vga_red;
    output vga_vsync;
    output RS_232_TX;
    input RS_232_RX;
    output sck;
    output miso;
    output reset_oled;
    output dc;   
        
    wire RX_done;
    wire [19:0]G_ROI_sum; //方框区域内G通道值的和，考虑精度问题，作FFT运算的数据不取平均值，而是取和的高16位   
    wire [10:0]HR_0; //16s窗口心率值
    wire HR_valid_0;
    wire [10:0]HR_1;//32s窗口心率值
    wire HR_valid_1;
    wire [10:0]HR_final;//16s与32s比较之后的心率值
    wire [3:0]red,green,blue;
    wire H_ROI_valid;
    wire[15:0] H_value;
    
    assign vga_red = red;
    assign vga_green = green;
    assign vga_blue = blue;

//H通道值计算
/*  
H_calculate H_calculate_inst(
    .clk(clk_in1),
    .pclk(OV7725_XCLK),
    .red(red),
    .green(green),
    .blue(blue),
    .H_ROI_valid(H_ROI_valid),
    .H_value(H_value)
    );
     
//16s窗口测心率           
BPM_calculate BPM_calculate_inst_0(
    .clk(clk_in1),
    .rst(rst),
    .G_ROI_sum(G_ROI_sum[19:4]),
    .flag(1'd0),
    .HR(HR_0),
    .HR_valid(HR_valid_0)
    );
 */   
//32s窗口测心率
      
BPM_calculate BPM_calculate_inst_1(
    .clk(clk_in1),
    .rst(rst),
    .G_ROI_sum(G_ROI_sum[19:4]),
    .flag(1'd1),
    .HR(HR_1),
    .HR_valid(HR_valid_1)
    );
/*
//16s与32s的结果比较    
HR_comparison HR_comparison_inst(
    .HR_0(HR_0),
    .HR_0_valid(HR_valid_0),
    .HR_1(HR_1),
    .HR_1_valid(HR_valid_1),
    .HR_final(HR_final)
    );           
*/
uart_top uart_inst (
    .clk_in1(clk_in1),
    .rst(rst),
    .HR_1(HR_1),
    .HR_valid_1(HR_valid_1),
    .RS_232_TX(RS_232_TX),
    .RS_232_RX(RS_232_RX),
    .RX_done(RX_done)    
    );
     
oled oled_inst(
    .clk_in1(clk_in1),
    .rst(rst),
    .HR_1(HR_1),
    .HR_valid_1(HR_valid_1),
    .RX_done(RX_done),
    .sck(sck),
    .miso(miso),
    .reset_oled(reset_oled),
    .dc(dc)
    );
        
 //ov7725摄像头顶层模块   
ov7725_vga_top ov7725_vga_top_inst(
    .OV7725_D(OV7725_D),
    .OV7725_HREF(OV7725_HREF),
    .OV7725_PCLK(OV7725_PCLK),
    .OV7725_SIOC(OV7725_SIOC),
    .OV7725_SIOD(OV7725_SIOD),
    .OV7725_VSYNC(OV7725_VSYNC),
    .OV7725_XCLK(OV7725_XCLK),
    .OV7725_PWDN(OV7725_PWDN),
    .OV7725_FSIN(OV7725_FSIN),
    .clk_in1(clk_in1),
    .rst(rst),
    .vga_blue(blue),
    .vga_green(green),
    .vga_hsync(vga_hsync),
    .vga_red(red),
    .vga_vsync(vga_vsync),
    .G_ROI_sum(G_ROI_sum),
    .H_ROI_valid(H_ROI_valid)
    );    
            
endmodule
